ARG FROM_TAG=latest
#ARG CALIBRE_VERSION=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/fedora:${FROM_TAG}
LABEL \
  org.opencontainers.image.title="Calibre" \
  org.opencontainers.image.description="Calibre"

#RUN echo Whuts da buzz... ;\
#   mount|grep tmpfs|grep -v devtmp|sort
RUN source /usr/libexec/RUN.sh; pkg_run \
	dbus-x11 python python-pip python-qt5-base python-qt5-webengine tar wget xdg-utils xz

RUN source /usr/libexec/RUN.sh ;\
  mkdir -p /usr/share/icons/hicolor &&\
  header "Installing Calibre..." ;\
  fetch https://download.calibre-ebook.com/linux-installer.sh | sh

#TODO
#VOLUMES
#PORTS
#ENV
#ETC

CMD ["/usr/bin/calibre-server"]
