ARG FROM_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/fedora:${FROM_TAG}
LABEL \
  org.opencontainers.image.title="MEGA" \
  org.opencontainers.image.description="MEGA"

COPY --chown=0:0 include/megasync.repo /etc/yum.repos.d/megasync.repo
RUN source /usr/libexec/RUN.sh;\
	pkg_run megacmd

ENV HOME=/var/mega
VOLUME /var/mega
CMD ["/usr/bin/mega-cmd-server"]
