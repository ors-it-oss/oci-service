ARG FROM_TAG=latest
ARG FROM_SCRIPTS_TAG=latest
FROM registry.gitlab.com/ors-it-oss/oci-base/scripts:${FROM_SCRIPTS_TAG} AS scriptsrc
FROM registry.gitlab.com/ors-it-oss/oci-base/fedora:${FROM_TAG}
LABEL \
  org.opencontainers.image.title="Salt" \
  org.opencontainers.image.description="Salt Shaker"

COPY --from=scriptsrc include/python /

ENV PIP_NO_CACHE_DIR=1
#Need to override socket creation in dir, but *how*
#COPY --chown=0:0 salt/include/gpg /
#  mv /usr/bin/gpg-agent /usr/bin/gpg-agent.orig &&\
#  mv /usr/local/bin/gpg-agent.sh /usr/bin/gpg-agent &&\
#  chmod 755 /etc/gpg && chmod 644 /etc/gpg/* &&\
#RUN \
RUN source /usr/libexec/RUN.sh ;\
  fetch -o /etc/bash_completion.d/salt https://raw.githubusercontent.com/saltstack/salt/master/pkg/salt.bash &&\
  echo 'salt:x:4505:salt' >>/etc/group &&\
  echo 'salt:x:4505:4505:Salt user:/etc/salt:/bin/sh' >>/etc/passwd &&\
  mkdir -m 750 /etc/salt && chown salt:salt /etc/salt
  # groupadd -rg 4505 salt &&\
  # useradd -rm -c "Salt user" -d /etc/salt -u 4505 -g 4505 salt

RUN source /usr/libexec/RUN.sh; pkg_run \
  bash-completion python python-m2crypto python-pip python-psutil python-pycryptodomex python-setuptools python-zmq sudo

#ENV HOME=/var/cache/salt
#Cython
ARG SALT_SOURCE=release
ARG SALT_VERSION
RUN source /usr/libexec/RUN.sh ;\
  case "${SALT_SOURCE}" in \
    git) pip_i="https://github.com/saltstack/salt/archive/${SALT_VERSION}.tar.gz" ;;\
    http*) pip_i="${SALT_SOURCE}/${SALT_VERSION}.tar.gz" ;;\
    *) pip_i="salt==${SALT_VERSION}" ;;\
  esac ;\
  pip_install "$pip_i" PyOpenSSL python-gnupg ;\
  chown -R salt:salt /etc/salt
#  chown -R salt:salt /{var/{cache,run},etc}/salt

RUN source /usr/libexec/RUN.sh; py_compile
VOLUME /var/cache/salt /var/run/salt /etc/salt/pki /var/log/salt

CMD ["/bin/bash", "-l"]
