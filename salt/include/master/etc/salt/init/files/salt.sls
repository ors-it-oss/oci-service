####---------------- Prepare salt master for execution ----------------####
Update Master extension cache:
  module.run:
  - saltutil.runner:
    - saltutil.sync_all

Accept own key:
  file.copy:
  - name: /etc/salt/pki/master/minions/{{ opts.id }}
  - source: /etc/salt/pki/minion/minion.pub

Tune Master:
  file.serialize:
  - name: /var/cache/salt/master/etc.d/tunes.conf
  - dataset_pillar: 'salt:tunes'
  - formatter: yaml
  - makedirs: True
