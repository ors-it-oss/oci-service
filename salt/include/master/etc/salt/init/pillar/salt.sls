{%- set worker_threads = grains.num_cpus * salt.environ.get('SALT_THREADS_PCPU', 3) %}
salt:
  tunes:
    worker_threads: {{ worker_threads }}
    max_open_files: {{ worker_threads * salt.environ.get('SALT_FILES_PTHREAD', 2048) }}
    batch_safe_limit: {{ worker_threads * 3 }}
    batch_safe_size: {{ worker_threads * 2 }}
