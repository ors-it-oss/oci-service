#!/bin/sh
set -e -o pipefail

salt-call --config-dir=/etc/salt/init state.highstate

exec salt-master
