== SaltStack

Containerized Salt

[options="header",frame=all,grid="cols",cols="<.<1%m,<.<50,<.<100"]
|==================================================================
| image       | tags | Description/notes
| salt        |      |
| salt-master |      |
|==================================================================

=== notes
* minion_id should become a function or somethin' with id_function. Do the same to master.
* GitFS backends don't precache jinja includes when file_client:local, same as in Salt-SSH boooh
* minion can't contact master @ localhost booooh
