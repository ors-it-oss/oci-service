ARG FROM_SALT_TAG
FROM registry.gitlab.com/ors-it-oss/oci-service/salt:${FROM_SALT_TAG}

LABEL \
  org.opencontainers.image.title="Salt Master" \
  org.opencontainers.image.description="Salt Shaker dedicated to Salt Master"

COPY --chown=0:0 include/master /
RUN source /usr/libexec/RUN.sh ;\
  for f in /etc/salt/* /etc/salt/init/*; do \
    n="$(echo "$f" | sed -E 's/\.(y[a]?ml|conf)$//')" ;\
    [[ "$f" != "$n" ]] || continue ;\
    mv -v "$f" "$n" ;\
  done &&\
  pkg_run python-pygit2 git-core &&\
	pip_install apache-libcloud &&\
  py_compile &&\
	chown -R salt:salt /etc/salt

VOLUME /srv/salt /etc/salt/master.d

ENTRYPOINT ["/usr/bin/ep.sh"]

EXPOSE 4505/tcp 4506/tcp
